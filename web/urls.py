from django.conf.urls import url
from .views import index, regis
urlpatterns = [
 url(r'^index', index, name='index'),
 url(r'^regis', regis, name='Regis'),
]